// Copyright 2024 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package main

import (
	"context"
	_ "embed"
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	"golang.org/x/text/message"
	"modernc.org/sqlite/lib"

	"mellium.im/cli"
	"mellium.im/communique/internal/localerr"
	"mellium.im/communique/internal/storage"
)

//go:embed schema.sql
var schema string

// Migrations is a collection of migrations for upgrading the database.
// It automatically checks the expected schema version of the application and
// orders itself to upgrade or downgrade the database to the correct version.
// The offset is the version,
func Migrations() storage.Migrations {
	return []storage.Migration{
		{
			Version: 1,
			Up:      schema,
			Down: `
			PRAGMA writable_schema = 1;
			DELETE FROM sqlite_master WHERE TYPE IN ('view', 'table', 'index', 'trigger');
			PRAGMA writable_schema = 0;`,
		},
	}
}

func dbCmd(appName, configPath, defAcct string, p *message.Printer, debug, logger *log.Logger) *cli.Command {
	const cmdName = "db"
	flags := flag.NewFlagSet(cmdName, flag.ContinueOnError)
	var dbFile string
	flags.StringVar(&dbFile, "db", dbFile, p.Sprintf("the database file to load"))

	return &cli.Command{
		Usage: cmdName,
		Description: strings.TrimSpace(p.Sprintf(`
Show information about the database. If no database file is selected the config
file is read to determine the default database for the active account. If the
selected file does not exist, a new database file is initialized.`)),
		Flags: flags,
		Run: func(c *cli.Command, _ ...string) error {
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()

			var account string
			if dbFile == "" {
				cfg, _, err := readConfig(configPath, p)
				if err != nil {
					return err
				}
				addr, acct, _, err := getDefAcct(cfg, defAcct, p)
				if err != nil {
					return err
				}
				account = addr.Bare().String()
				dbFile = acct.DB
			}

			db, err := storage.OpenDB(ctx, appName, account, dbFile, nil, p, debug)
			if err != nil {
				return localerr.Wrap(p, "error opening database: %v", err)
			}
			defer db.Close()

			var currentID uint
			err = db.QueryRowContext(ctx, "PRAGMA user_version").Scan(&currentID)
			if err != nil {
				return err
			}
			latest := Migrations().Latest().Version
			fmt.Print(p.Sprintf(`Database Information

sqlite3 version:  %s
database file:    %s
schema version:   %d
latest schema:    %d
`,
				sqlite3.SQLITE_VERSION, db.DBPath, currentID, latest))

			return nil
		},
	}
}
