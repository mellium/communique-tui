// Copyright 2021 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package storage

import (
	"context"
	"database/sql"
	"iter"
)

// RowIter returns an iterator that pulls from a set of database rows and
// unmarshals each row into a concrete type.
//
// The cancel function is called when the iteration is completed.
// It can be used to signal the end of iteration to any cleanup goroutines that
// may be running (ie. to release locks on the database when the query is
// complete).
// Rows will also be closed when the iteration is complete.
// If an error is returned while closing the rows an extra iteration is added
// with the zero value of V and the error.
func RowIter[V any](cancel context.CancelFunc, rows *sql.Rows, f func(*sql.Rows) (V, error)) iter.Seq2[V, error] {
	return func(yield func(V, error) bool) {
		defer func() {
			err := rows.Close()
			if err != nil {
				yield(*new(V), err)
			}
			cancel()
		}()
		for {
			next := rows.Next()
			if !next {
				return
			}
			next = yield(f(rows))
			if !next {
				return
			}
		}
	}
}
